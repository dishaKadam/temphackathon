import { ProjectsEmployeesModule } from './projects-employees.module';

describe('ProjectsEmployeesModule', () => {
  let projectsEmployeesModule: ProjectsEmployeesModule;

  beforeEach(() => {
    projectsEmployeesModule = new ProjectsEmployeesModule();
  });

  it('should create an instance', () => {
    expect(projectsEmployeesModule).toBeTruthy();
  });
});
