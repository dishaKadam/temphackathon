import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsEmployeesComponent } from './projects-employees.component';

const routes: Routes = [
  {path:'', component: ProjectsEmployeesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectsEmployeesRoutingModule { }
