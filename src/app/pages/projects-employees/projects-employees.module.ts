import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsEmployeesRoutingModule } from './projects-employees-routing.module';
import { ProjectsEmployeesComponent } from './projects-employees.component';

@NgModule({
  imports: [
    CommonModule,
    ProjectsEmployeesRoutingModule
  ],
  declarations: [ProjectsEmployeesComponent]
})
export class ProjectsEmployeesModule { }
