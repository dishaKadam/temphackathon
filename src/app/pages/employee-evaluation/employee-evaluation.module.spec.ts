import { EmployeeEvaluationModule } from './employee-evaluation.module';

describe('EmployeeEvaluationModule', () => {
  let employeeEvaluationModule: EmployeeEvaluationModule;

  beforeEach(() => {
    employeeEvaluationModule = new EmployeeEvaluationModule();
  });

  it('should create an instance', () => {
    expect(employeeEvaluationModule).toBeTruthy();
  });
});
