import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeEvaluationRoutingModule } from './employee-evaluation-routing.module';
import { EmployeeEvaluationComponent } from './employee-evaluation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    EmployeeEvaluationRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [EmployeeEvaluationComponent]
})
export class EmployeeEvaluationModule { }
