import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Rating } from './app/rating';
import { Form, NgForm } from '../../../../node_modules/@angular/forms';
// import {  } from '@angular/forms';

@Component({
  selector: 'app-employee-evaluation',
  templateUrl: './employee-evaluation.component.html',
  styleUrls: ['./employee-evaluation.component.css']
})
export class EmployeeEvaluationComponent implements OnInit {
  @ViewChild("webApplication") webApplication : ElementRef;

  helpfulness : any;
  problemSolving : any;
  workQuality : any;
  companyValue: any;
  absenteeism : any;
  workEfficiency : any;
  jiraCount : any;
  communication : any;
  selfLearner : any;
  multiTasking : any;
  knowledge : any;
  reliability : any;
  isValidForm = false;

  percent = '80%';
  model = new Rating();

  constructor( ) { }

  ngOnInit() {
  //  console.log("Helpfulness : " , this.helpfulness);
  }

  ratingHelpfulness(value){
    console.log(value);
    this.helpfulness = value;
    this.checkFormValidity();
  }

  ratingProblemSolving(value){
    console.log(value);
    this.problemSolving = value;
    this.checkFormValidity();
  }

  ratingWorkQuality(value){
    console.log(value);
    this.workQuality = value;
    this.checkFormValidity();
  }

  ratingCompanyValues(value){
    console.log(value);
    this.companyValue = value;
    this.checkFormValidity();
  }

  ratingAbsenteeism(value){
    console.log(value);
    this.absenteeism = value;
    this.checkFormValidity();
  }
  ratingWorkEfficiency(value){
    console.log(value);
    this.workEfficiency = value;
    this.checkFormValidity();
  }
  ratingJiraCount(value){
    console.log(value);
    this.jiraCount = value;
    this.checkFormValidity();
  }
  ratingCommunication(value){
    console.log(value);
    this.communication = value;
    this.checkFormValidity();
  }

  ratingSelfLearner(value){
    console.log(value);
    this.selfLearner = value;
    this.checkFormValidity();
  }
  ratingMultiTasking(value){
    console.log(value);
    this.multiTasking = value;
    this.checkFormValidity();
  }
  ratingKnowledge(value){
    console.log(value);
    this.knowledge = value;
    this.checkFormValidity();
  }
  ratingRelliabillity(value){
    console.log(value);
    this.reliability = value;
    this.checkFormValidity();
  }

  progressBarPercent(event){
    //  this.webApplication.nativeElement.width = this.percent;
    //  console.log(this.webApplication.nativeElement.class);
  }

  submit(form : NgForm){

      console.log(form);
      console.log(this.helpfulness);
      console.log(this.problemSolving);
      console.log(this.workQuality);
      console.log(this.companyValue);
      console.log(this.absenteeism);
      console.log(this.workEfficiency);
      console.log(this.jiraCount);
      console.log(this.communication);
      console.log(this.selfLearner);
      console.log(this.multiTasking);
      console.log(this.knowledge);
      console.log(this.reliability);
      form.resetForm();

  }

  checkFormValidity(){
      if( (this.helpfulness == undefined || this.helpfulness == null) ||  (this.problemSolving == undefined || this.problemSolving == null) ||  (this.workQuality == undefined || this.workQuality == null) ||
      (this.companyValue == undefined || this.companyValue == null) ||  (this.absenteeism == undefined || this.absenteeism == null) ||  (this.workEfficiency == undefined || this.workEfficiency == null) || 
      (this.jiraCount == undefined || this.jiraCount == null) ||  (this.communication == undefined || this.communication == null) ||  (this.selfLearner == undefined || this.selfLearner == null) || 
      (this.multiTasking == undefined || this.multiTasking == null) ||  (this.knowledge == undefined || this.knowledge == null) ||  (this.reliability == undefined || this.reliability == null)){
            this.isValidForm = false;
      }else{
            this.isValidForm = true;
      }
  }

}
