import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeEvaluationComponent } from './employee-evaluation.component';

const routes: Routes = [
  {path: '', component: EmployeeEvaluationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeEvaluationRoutingModule { }
