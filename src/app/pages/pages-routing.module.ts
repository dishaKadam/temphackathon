import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';

const routes: Routes = [
  {path: '', redirectTo : '/dashboard', pathMatch:'full'},
  {
    path: '', component : PagesComponent, children:[     
        { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule'},
        { path: 'projects-employees', loadChildren: './projects-employees/projects-employees.module#ProjectsEmployeesModule'},
        { path: 'newsfeed', loadChildren: './newsfeed/newsfeed.module#NewsfeedModule'},
        { path: 'employee-evaluation', loadChildren: './employee-evaluation/employee-evaluation.module#EmployeeEvaluationModule'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
